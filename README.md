1. Required 
- Node JS
https://nodejs.org/en/download/

- .Net 5.0
https://dotnet.microsoft.com/en-us/download/dotnet/5.0


2. Edit setting in file appsetting.json

- UPC.Salt to sign and verify signature.
- UPC.APIKey to the key that you receive from UPC.


3. Run
- Open solution with Visual Studio
- Start with debug or without debug.


========================================================================

Live Demo
https://uat-merchant.galaxypay.vn

Integration Documents
https://gitlab.com/gpay-sample/upc/upc-dotnet/-/tree/master/Documents